본 저장소를 clone받아 ~/.emacs.d/ 디렉토리로 삼는다. 그리고 아래 명령을 실행한다.

아래 명령으로 Cask를 설치한다.

```
make init
```

아래 명령으로 Cask를 통해 패키지들을 설치한다.

```
make cask
```

그리고 외부 라이브러리를 설치한다.

```
make external
```
