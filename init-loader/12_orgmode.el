(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (emacs-lisp . t)
   (R . t)
   (ditaa . t)
   ))

(setq org-confirm-babel-evaluate nil)
